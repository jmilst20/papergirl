﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Intro : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject textBox = GameObject.Find("day");
		if(textBox != null){
			textBox.GetComponent<Text>().text = GameManager.Instance.getDay();
		}	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
