﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public GameObject toFollow;
	private float smooth = 100;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate () {
		if(toFollow != null){
			Vector3 specificVector = new Vector3(0, toFollow.transform.position.y, transform.position.z);
			transform.position = Vector3.Lerp(transform.position , specificVector, smooth * Time.deltaTime);
		}
	}
}
