﻿using UnityEngine;
using System.Collections;

public class carMovement : MonoBehaviour {
	private float startX;
	private Rigidbody2D body;
	public bool isReversed = false;
	private float speed = 5;

	// Use this for initialization
	void Start () {
		startX = transform.position.x;
		body = gameObject.GetComponent<Rigidbody2D>();
		if(isReversed){
			speed = -10;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(body != null){
			
			if(body.velocity.x < 15 && body.velocity.x > -15){
				body.AddForce(transform.right * speed);
			}
		
			if(transform.position.x > 50 || transform.position.x < -50){
				Vector3 pos = transform.position;
				pos.x = startX;
				transform.position = pos;

			}
		}
	}
}
