﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GameManager : Singleton<GameManager> {

	private GameState gameState; 
	private string[] DAYS = new string[7] {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	private const int MAX_PAPERS = 10;
	protected GameManager(){
		updateState(new GameState());
	}

	public void Start(){

	}
	
	public void Awake(){

	}
		
	public void Update(){
		//do update stuff

	}

	private void updateState(GameState newState){
		gameState = newState;
	}

	public void resetGame(){
		updateState(new GameState());
		showNextIntro();
	}

	public void resetLevel(){

	}

	public void showDeath(){
		resetLevel();
	}


	public void showNextIntro(){
		Application.LoadLevel("intro");
		StartCoroutine(showLevel());
	}
	
	private IEnumerator showLevel(){
		yield return new WaitForSeconds(2);
		Application.LoadLevel("level1");
	}

	public void showGameWon(){
		Application.LoadLevel("menu");
	}

	public void levelComplete(){
		
		updateState(new GameState { 
			lives = gameState.lives, 
			currentDay = gameState.currentDay + 1,
			papers = MAX_PAPERS // reset the papers
		});

		if(gameState.currentDay < 7){
			showNextIntro();
		}else{
			showGameWon();
		}

	}

	public int getLives(){
		return gameState.lives;
	}

	public string getDay(){
		return DAYS[gameState.currentDay];
	}

	public int getPapers(){
		return gameState.papers;
	}

	public void removeLife(){
		
		updateState(new GameState { 
			lives = gameState.lives - 1, 
			currentDay = gameState.currentDay,
			papers = gameState.papers
		});

		if(gameState.lives <= 0){
			showDeath();
		}
	}

	public void addPapers(){
		
		updateState(new GameState { 
			lives = gameState.lives, 
			currentDay = gameState.currentDay,
			papers = MAX_PAPERS
		});

	}

	public void removePaper(){
		updateState(new GameState { 
			lives = gameState.lives, 
			currentDay = gameState.currentDay,
			papers = (gameState.papers > 0) ? gameState.papers - 1 : 0
		});
	}


	public class GameState{
		public int lives = 3;
		public int currentDay = 0;
		public int papers = MAX_PAPERS;
	}
}