﻿using UnityEngine;
using System.Collections;

public class VerticalCar : MonoBehaviour {
	private Rigidbody2D body;
	public bool isReversed = false;
	private float speed = 5;
	private bool stop = false;

	// Use this for initialization
	void Start () {
		body = gameObject.GetComponent<Rigidbody2D>();
		if(isReversed){
			speed = -10;
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		if(body != null){
			if(!stop){
				if(body.velocity.y < 10 && body.velocity.y > -10){
					body.AddForce(transform.up * -speed);
				}
			}else{
				body.velocity = new Vector2(0,0);
			}

			if(transform.position.y < -30){
				Vector3 pos = transform.position;
				pos.y = 140;
				transform.position = pos;

			}

			stop = false;
		}
	}


	void OnTriggerStay2D(Collider2D other)
	{
		if(other.gameObject.tag == "CAR"){
			stop = true;
		}   
	}

}
