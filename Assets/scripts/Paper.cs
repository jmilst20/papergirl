﻿using UnityEngine;
using System.Collections;

public class Paper : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col){
		Rigidbody2D body = GetComponent<Rigidbody2D>();
		body.velocity = new Vector2(0,0);
		body.freezeRotation = true;

	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "TARGET"){
			Rigidbody2D body = GetComponent<Rigidbody2D>();
			body.velocity = new Vector2(0,0);
			body.freezeRotation = true;

			Vector2 pos = transform.position;
			pos.x = col.transform.position.x;
			pos.y = col.transform.position.y;
			transform.position = pos;

			col.GetComponent<Animator>().SetBool("hit", true);
		}
	}
}
