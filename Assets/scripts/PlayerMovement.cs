﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	private float speed = 50;
	public GameObject projectile;
	private float shootForce = 80;
	private Rigidbody2D body;
	private bool throwEnabled = true;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = this.transform.position;

		//this makes the player move faster 
		if(Input.GetKey(KeyCode.W)){
			body.AddForce(transform.up * speed * 2);
		}else{
			body.AddForce(transform.up * speed);
		}

		//moves the player left and right
		if(Input.GetKey(KeyCode.A)){
			body.AddForce(transform.right * -speed);
		}else if(Input.GetKey(KeyCode.D)){
			body.AddForce(transform.right * speed);
		}

		this.transform.position = pos;


		if(GameManager.Instance.getPapers() > 0 && throwEnabled){

			//check if you should throw the paper
			bool throwLeft = ((Input.touchCount > 0 && Input.GetTouch(0).position.x < Screen.width/2) || Input.GetKeyDown(KeyCode.LeftShift));
			bool throwRight = ((Input.touchCount > 0 && Input.GetTouch(0).position.x > Screen.width/2) || Input.GetKeyDown(KeyCode.RightShift));
			if(throwLeft || throwRight){
				throwPaper(throwLeft);
			}
		}
			
	}

	void FixedUpdate(){
		Vector3 movement = new Vector3 (Input.acceleration.x / 10, (Input.acceleration.z < 0) ? -Input.acceleration.z / 20 : 0 , 0.0f);
		this.GetComponent<Rigidbody2D>().velocity = movement * speed;
	}

	private IEnumerator resetThrow(){
		yield return new WaitForSeconds(0.4F);
		throwEnabled = true;
	}

	private void throwPaper(bool throwLeft){
		Vector2 newPos = transform.position;
		if(throwLeft){
			newPos.x -= 1;
		}else{
			newPos.x += 1;
		}

		// Instantiate the projectile at the position and rotation of this transform
		Transform clone = (Transform)Instantiate(projectile.transform, newPos, transform.rotation);

		// Add force to the cloned object in the object's forward direction
		Rigidbody2D paperBody = clone.GetComponent<Rigidbody2D>();

		if(throwLeft){
			paperBody.AddForce(clone.transform.right * -shootForce * 2);
		}else{
			paperBody.AddForce(clone.transform.right * shootForce * 2);
		}
		paperBody.AddForce(clone.transform.up * shootForce/2);

		GameManager.Instance.removePaper();
		throwEnabled = false;
		StartCoroutine(resetThrow());

	}

}
